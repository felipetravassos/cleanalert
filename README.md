# Bootstrap Clean Alert


A plugin to print beautiful and clean alert modals.


Demo: http://felipetravassos.com/cleanalert/demo.html

Version - 0.4
Copyright (c) 2019 Felipe Travassos
